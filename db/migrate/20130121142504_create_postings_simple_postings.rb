class CreatePostingsSimplePostings < ActiveRecord::Migration
  def change
    create_table :postings_simple_postings do |t|
      t.string :name

      t.timestamps
    end
  end
end
