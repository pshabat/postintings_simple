$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "postings_simple/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "postings_simple"
  s.version     = PostingsSimple::VERSION
  s.authors     = 'Pavlo Shabat'
  s.email       = 'pavlo@shabat@perfectial.com'
  #s.homepage    = "TODO"
  s.summary     = "This is posting application"
  s.description = "This is posting application"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 3.2.11"
  # s.add_dependency "jquery-rails"

  s.add_development_dependency "pg"
end
